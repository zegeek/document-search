#include "existsAt.h"

std::size_t existsAt(std::vector<Word> words, Word word)
{
	size_t iter = 0;
	bool found = false;
	while (iter < words.size() && !found)
	{
		if (words.at(iter).getWord() == word.getWord())
			found = true;
		else
			iter++;
	}
	return iter;
}
