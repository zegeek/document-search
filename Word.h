#pragma once
#include <iostream>
#include <fstream>
#include <string>

class Word
{
	std::string word;
	int frequency;
public:
	Word();
	Word(std::string word, int frequency);
	Word(const Word& object);
	void setWord(std::string word);
	void setFrequency(int frequency);
	void incrementFrequency();
	std::string getWord() const;
	int getFrequency() const;
	friend std::ostream& operator<<(std::ostream& flux, const Word& object);
	~Word();
};

