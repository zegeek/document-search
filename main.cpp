#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <omp.h>
#include "Word.h"
#include "Document.h"
#include "scandocument.h"
#include "scanfile.h"
#include "extractdictionary.h"
#include "getfilevector.h"

using namespace std;

int main(int argc, char **argv)
{
	double time_init = omp_get_wtime();
	vector<Document> documents;
	Document document;
//#pragma omp parallel for
	for (int i = 1; i <= TOTAL; i++)
	{
		scanfile("hello" + to_string(i) + ".txt");
	}
//#pragma omp parallel for private(document)
	for (int i = 1; i <= TOTAL; i++)
	{
		document = scandocument("hello" + to_string(i) + ".txt.dat");
//#pragma omp critical
			//{
				documents.push_back(document);
			//}
	}
	extractdictionary(documents);
//#pragma omp parallel for
	for (int i = 0; i < TOTAL; i++)
	{
		getfilevector(documents.at(i));
	}
	cout << "Done in " << omp_get_wtime() - time_init << "s"<<endl;
	cout << "Press ENTER to continue...";
	cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	return 0;
}