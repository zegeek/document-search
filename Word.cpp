#include "Word.h"



Word::Word()
{
}

Word::Word(std::string word, int frequency)
{
	this->word = word;
	this->frequency = frequency;

}
Word::Word(const Word& object)
{
	this->word = object.word;
	this->frequency = object.frequency;
}
void Word::setWord(std::string word)
{
	this->word = word;
}
void Word::setFrequency(int frequency)
{
	this->frequency = frequency;
}
void Word::incrementFrequency()
{
	this->frequency += 1;
}
std::string Word::getWord() const
{
	return this->word;
}
int Word::getFrequency() const
{
	return this->frequency;
}
std::ostream& operator<<(std::ostream& flux, const Word& object)
{
	 flux << object.word << " * " << object.frequency;
	return flux;
}

Word::~Word()
{
}
