#pragma once
#include "Word.h"
#include <string>
#include <vector>

std::size_t existsAt(std::vector<Word> words, Word word);