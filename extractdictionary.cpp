#include "extractdictionary.h"

using namespace std;

void extractdictionary(vector<Document> documents)
{
	fstream file;
	vector<Word> words;
	size_t iter = 0;
	for (iter = 0; iter < documents.size(); iter++)
	{
		for (size_t i = 0; i < documents.at(iter).getWordsAmount(); i++)
		{
			if (!exists(words, documents.at(iter).getWordAt(i)))
				words.push_back(Word(documents.at(iter).getWordAt(i).getWord(),1));
			else
			{
				words.at(existsAt(words, documents.at(iter).getWordAt(i))).incrementFrequency();
			}
		}
	}
	file.open("datafiles/index.df", fstream::out);
	if (file.is_open())
	{
		for (size_t i = 0; i < words.size(); i++)
		{
			file << words.at(i)<<endl;
		}
	}
}