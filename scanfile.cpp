#include "scanfile.h"

using namespace std;

void scanfile(string file_name)
{
	ifstream file;
	ofstream dat_file;
	string line,buffer,word;
	size_t position;
	size_t iter = 0;
	bool found = false;
	vector<Word> words;
	file.open(file_name);
	if (file.is_open())
	{
		while (getline(file,line))
		{
			while (line.length() > 0)
			{
				iter = 0;
				found = false;
				position = line.find(" ");
				if (position != string::npos)
				{
					buffer = line.substr(0, position);
					line = line.substr(position + 1, line.length());
					buffer = clean_buffer(buffer);
				}
				else
				{
					buffer = line;
					buffer = clean_buffer(buffer);
					line = "";
				}
				while ( iter < words.size() && !found)
				{
					if (words.at(iter).getWord() == buffer)
					{
						words.at(iter).setFrequency(words.at(iter).getFrequency() + 1);
						iter = 0;
						found = true;
					}
					else
					{
						iter++;
					}
				}
				if (!found)
					words.push_back(Word(buffer, 1));
			}
		}
	}
	file.close();
	dat_file.open("datafiles/"+file_name + ".dat");
	for (iter = 0; iter < words.size(); iter++)
	{
		dat_file << words.at(iter) << endl;
	}
}
