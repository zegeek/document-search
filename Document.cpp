#include "Document.h"

using namespace std;

Document::Document()
{
}

Document::Document(string name, vector<Word> words)
{
	this->name = name;
	this->words = words;
}

string Document::getName() const
{
	return this->name;
}

bool Document::searchIndexation(string word)
{
	bool found = false;
	size_t iter = 0;
	while (iter < this->words.size() && !found)
	{
		if (this->words.at(iter).getWord() == word)
		{
			found = true;
		}
		else
		{
			iter++;
		}
	}
	return found;
}

size_t Document::getWordsAmount() const
{
	return this->words.size();
}

int Document::getWordFrequency(std::string word) const
{
	bool found = false;
	size_t iter = 0;
	while (iter < this->getWordsAmount() && !found)
	{
		if (this->words.at(iter).getWord() == word)
		{
			return this->words.at(iter).getFrequency();
			found = true;
		}
		else
			iter++;
	}
	return 0;
}

Word Document::getWordAt(size_t position) const
{
	return this->words.at(position);
}

Document::~Document()
{
}
