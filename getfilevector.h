#pragma once
#include <fstream>
#include <math.h>
#include <iostream>
#include <omp.h>
#include "Word.h"
#include "Document.h"
#include "exists.h"
#include "existsAt.h"
#include "scandocument.h"

#ifndef TOTAL
#define TOTAL 6
#endif

void getfilevector(Document document);