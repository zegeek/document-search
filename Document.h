#pragma once
#include "Word.h"
#include <vector>

class Document
{
	std::string name;
	std::vector<Word> words;
public:
	Document();
	Document(std::string name, std::vector<Word> words);
	std::string getName() const;
	bool searchIndexation(std::string word);
	size_t getWordsAmount() const;
	int getWordFrequency(std::string word) const;
	Word getWordAt(size_t position) const;
	~Document();
};

