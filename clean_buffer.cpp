#include "clean_buffer.h"
#include <string>
using namespace std;


string clean_buffer(string buffer)
{
	string separator[] = { " ",";",",","\n","\t","!","?",".",":" ,"(",")"};
	size_t position = 0;
	for (short int i = 0; i < SEP_AM; i++)
	{
		position = buffer.find(separator[i]);
		if (position != string::npos)
			if(position > 0)
				buffer = buffer.substr(0, position);
			else if(position == 0)
				buffer = buffer.substr(position+1, buffer.length());
	}
	return buffer;
}