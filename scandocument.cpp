#include "clean_buffer.h"
#include "scandocument.h" 

using namespace std;

Document scandocument(string file_name)
{
	fstream file;
	string line, buffer;
	size_t position = 0;
	Document documents;
	vector<Word> words;
	file.open("datafiles/" + file_name, fstream::in);
	if (file.is_open())
	{
		while (getline(file, line))
		{
			position = line.find("*");
			if (position != string::npos)
			{
				buffer = line.substr(0, position);
				line = line.substr(position+1, line.length());
			}
				
			buffer = clean_buffer(buffer);
			line = clean_buffer(line);
			words.push_back(Word(buffer, stoi(line)));
		}
	}
	file.close();
	return Document(file_name, words);
}