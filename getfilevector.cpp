#include "getfilevector.h"

using namespace std;

void getfilevector(Document document)
{
	double index = 0;
	string vector_line;
	ofstream file;
	int redundancy = 0;
	Document indexdf = scandocument("index.df");
	vector_line.append(document.getName());
	vector_line.append(" { ");
	for (size_t iter = 0; iter < document.getWordsAmount(); iter++)
	{
		redundancy = indexdf.getWordFrequency(document.getWordAt(iter).getWord());
		index = ((double) document.getWordAt(iter).getFrequency()) * log(((double)TOTAL / (double)redundancy));
		vector_line.append("( "+document.getWordAt(iter).getWord() + "," + to_string(index) + " ),");
	}
	vector_line.append(" }");

#pragma omp critical
	{
		file.open("datafiles/vectorised.vec", fstream::app);
		if (file.is_open())
		{
			file << vector_line << endl;
		}
		file.close();
	}
}
